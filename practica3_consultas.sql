﻿USE practica3;

                                             /* PRACTICA NUMERO 3 */

/* 1- Visualizar el número de empleados de cada departamento.
Utilizar GROUP BY para agrupar por departamento */

SELECT
 e.dept_no,COUNT(*) n 
FROM
 emple e
GROUP BY e.dept_no
;

/* 2. Visualizar los departamentos con más de 5 empleados.
Utilizar GROUP BY para agrupar por departamento y HAVING para establecer la condición sobre los grupos */
